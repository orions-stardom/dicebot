import pytest
import dice


@pytest.mark.parametrize("n_sides", [6, 20, 100, 1000])
def test_arb_single_dice(n_sides):
    result = int(dice.roll_sum(f"d{n_sides}"))
    assert 1 <= result <= n_sides


@pytest.mark.parametrize("n_dice", [1, 3, 5, 10, 100])
@pytest.mark.parametrize("n_sides", [6, 20, 100, 1000])
def test_arb_multi_dice(n_dice, n_sides):
    result = int(dice.roll_sum(f"{n_dice}d{n_sides}"))
    assert n_dice <= result <= n_sides * n_dice


@pytest.mark.parametrize("command,min_,max_", [("d20+d20+3", 5, 43)])
def test_sum(command, min_, max_):
    message = dice.roll_sum(command)
    result = int(message)
    assert min_ <= result <= max_


@pytest.mark.parametrize("mode", "bw")
@pytest.mark.parametrize("n_dice", [1, 3, 5, 10, 100])
@pytest.mark.parametrize("n_sides", [6, 20, 100, 1000])
def test_multi_dice_modes(n_dice, n_sides, mode):
    command = f"{n_dice}d{mode}{n_sides}"
    result = int(dice.roll_sum(command))

    assert 1 <= result <= n_sides
