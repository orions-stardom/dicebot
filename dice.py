#! /usr/bin/env python3

import random, re
import discordbot

import os

bot = discordbot.Bot(
    channels={int(channel) for channel in os.getenv("CHANNELS", "").splitlines()},
    discord_token=os.environ["DISCORD_TOKEN"],
)


@bot.text_replace("r")
def roll_sum(message):
    """
    Roll some dice

    You can use any of the following dice formats:
    - **Single dice:** `d20` will roll a single 20-sided die. Any (integer) number of sides is supported
    - **Multiple dice:** `5d20` will roll five 20-sided dice and give the sum of the results
    - **Best of n dice:** `5db20` will roll five 20-sided dice and give the single best roll, discarding the others
    - **Worst of n dice:** `5dw20` will roll five 20-sided dice and give the single worst roll, discarding the others
    - **Just a number:** `20` will just be the number 20
    - Chain any of these together with '+' signs to, well, add them together

    """
    return str(sum(roll(r) for r in message.split("+")))


_roll_pattern = re.compile(r"(?P<num>[0-9]*)d(?P<mode>[bw]?)(?P<sides>[0-9]+)")
_roll_modes = {"": sum, "b": max, "w": min}


def roll(command):
    if command.isdigit():
        return int(command)

    num, mode, sides = _roll_pattern.fullmatch(command).groups()
    num = int(num) if num else 1

    return _roll_modes[mode](random.randint(1, int(sides)) for _ in range(num))
