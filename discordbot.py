import logging, discord
import re
import textwrap

logging.basicConfig(
    level=logging.INFO,
    force=True,
    format="%(asctime)-15s|%(name)-20s|%(levelname)-7s: %(message)s",
)


class Bot(discord.Client):
    def __init__(self, channels, discord_token, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.channels = channels
        self.discord_token = discord_token

        # don't match any text replace triggers until some are registered
        self.textreplace_funcs = {}
        self.textreplace_pattern = re.compile("$.^")  # no possible matches

        self.commands = {"help": self.help}

    @property
    def mention(self):
        return f"<@!{self.user.id}>"

    async def on_ready(self):
        logging.info("Logged on as %s", self.user)

    async def on_message(self, message):
        if message.author == self.user:  # don't respond to ourselves
            return
        if message.channel.id not in self.channels:
            return

        if message.content.startswith(self.mention):
            content = message.content.removeprefix(self.mention).strip().split(None, 1)
            command, arg = content if len(content) > 1 else (content[0], "")

            if command not in self.commands:
                response = f"No such command: {command}. `{self.mention} help` for help"
            else:
                response = self.commands[command](arg)

            if "\n" in response:
                response = "\n" + response

            await message.channel.send(f"<@{message.author.id}> {response}")

        else:
            new_message = self.textreplace_pattern.sub(
                self._do_textreplace, message.content
            )

            if new_message != message.content:
                await message.channel.send(f"<@{message.author.id}>\n>>> {new_message}")

    def help(self, about):
        """
        Show some help text.

        """
        # If we're asked about something in particular, give all the details about it
        if about:
            if about.startswith("/"):
                thing = about[1:]
                where = self.textreplace_funcs
            else:
                thing = about
                where = self.commands

            if thing in where:
                heading, doc = where[thing].__doc__.strip().split("\n", 1)

                return f"__**{about}: {heading}**__\n" + textwrap.dedent(doc)

            else:
                return "I can't help you with that. Sorry."

        # Otherwise give a summary of everything
        commands = "\n".join(
            f"**{command}:** {func.__doc__.strip().splitlines()[0]}"
            for command, func in self.commands.items()
        )
        replace = "\n".join(
            f"**/{trigger}:** {func.__doc__.strip().splitlines()[0]}"
            for trigger, func in self.textreplace_funcs.items()
        )

        return (
            f"Available commands (to use: {self.mention} <command> <arguments>)\n"
            f"{commands}\n"
            "\n"
            "Available text replacements (include these in any message and I will echo back that message with replacements made\n"
            f"{replace}"
        )

    def _do_textreplace(self, match):
        # re.sub callback for text replacement
        trigger, text = match.groups()
        return f"`{text}`->`{self.textreplace_funcs[trigger](text)}`"

    def text_replace(self, trigger):
        """
        Decorator to register a function as a text replacement command

        Text replace will be triggered on any whitespace-delimeted word in a
        message that starts with '/' followed by `trigger`, and the remainder
        of that word will be passed to the decorated function, which should
        the replacement value.

        """

        def inner(func):
            self.textreplace_funcs[trigger] = func
            self.textreplace_pattern = re.compile(
                fr"(?:\A|\s)/({'|'.join(self.textreplace_funcs)})(\S+)"
            )
            return func

        return inner

    def command(self, func):
        """
        Decorator to register a new command.

        The command will have the same name as the function, and will be triggered by
        a mention of the bot name followed by the command. The rest of the message after
        the command name will be passed to the function as a single string.

        """
        self.commands[func.__name__] = func

    def __call__(self):
        self.run(self.discord_token)
